package d14.pointDistance;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static Scanner scanner = new Scanner(System.in);
    public static List<Point> pointList = new ArrayList<>();

    public static void main(String[] args) {
        addPoints();
        sortList();
        printList(pointList);
    }

    private static void addPoints() {
        System.out.println("how many points you want to check?");
        int value = scanner.nextInt();
        for (int i = 0; i < value; i++) {
            System.out.println("Podaj nazwe punktu " + i);
            scanner.nextLine();
            String pointName = scanner.nextLine();
            System.out.println("Podaj x dla punktu " + i);
            double x = scanner.nextInt();
            System.out.println("Podaj y dla punktu " + i);
            double y = scanner.nextInt();
            Point point = new Point(pointName, x, y);
            pointList.add(point);
        }
    }

    public static void sortList(){
        pointList.sort((o1, o2) -> (int)(o1.getDistance()-o2.getDistance()));
    }

    public static void printList(List<Point> pointList) {
        for (int i = 0; i < pointList.size(); i++) {
            System.out.println(pointList.get(i)+ " distance = "+ pointList.get(i).getDistance());
            System.out.println();
        }
    }
}
